#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <set>
#include <string>
#include <functional>

// Define a struct for holding the client ID and the date
struct ClientDate {
    std::string client_id;
    std::string date;

    bool operator==(const ClientDate& other) const {
        return client_id == other.client_id && date == other.date;
    }
};

// Define a custom hash function for ClientDate
namespace std {
    template<>
    struct hash<ClientDate> {
        size_t operator()(const ClientDate& cd) const {
            return hash<std::string>()(cd.client_id) ^ hash<std::string>()(cd.date);
        }
    };
}

int main() {
    std::ifstream file("index.csv"); // Replace with your file path
    std::string line;

    
    std::getline(file, line);

    std::unordered_map<ClientDate, std::set<std::string>> client_day_songs;
    int max_songs = 0; // Variable to track the maximum number of distinct songs played

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string play_id, song_id, client_id, play_ts;

        std::getline(iss, play_id, '\t');
        std::getline(iss, song_id, '\t');
        std::getline(iss, client_id, '\t');
        std::getline(iss, play_ts, '\t');

        
        std::string date = play_ts.substr(0, 10);

       
        client_day_songs[{client_id, date}].insert(song_id);

        
        max_songs = std::max(max_songs, static_cast<int>(client_day_songs[{client_id, date}].size()));
    }

    
    int count_346 = 0;
    for (const auto &pair : client_day_songs) {
        if (pair.second.size() == 346) {
            count_346++;
        }
    }

    std::cout << "Number of clients who played 346 distinct songs: " << count_346 << std::endl;
    std::cout << "Maximum number of distinct songs played by any client: " << max_songs << std::endl;

    return 0;
}
